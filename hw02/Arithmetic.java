////// Michalina Modzelewska 
/// CSE02 hw02 Arithmetic Calculations
// September 11th, 2018

public class Arithmetic{
  
  public static void main(String args[]){
     
    int numPants = 3;
    //number of pairs of pants
    double pantsPrice = 34.98; 
    //cost per pair of pants 
    int numShirts = 2;
    //number of sweatshirts
    double shirtsPrice = 24.99;
    //cost per shirt
    int numBelts = 1;
    //number of belts
    double beltCost = 33.99;
    //cost per belt
    
    
    double paSalesTax = 0.06;
    //the tax rate
    double totalCostOfPants; 
    //total cost of pants
    double totalCostOfShirts; 
    //total cost of sweatshirts
    double totalCostOfBelts; 
    //total cost of belts 
    
    totalCostOfPants = (numPants * pantsPrice); 
    //total cost of pants is equal to number of pants multiplied by the price of pants 
    
    totalCostOfShirts = (numShirts * shirtsPrice);
    //total cost of sweatshirts is equal to number of sweatshirts multiplied by the price of sweatshirts
    
    totalCostOfBelts = (numBelts * beltCost);  
    //total cost of belts is equal to number of belts multiplied by the price of belts 
    
    
    
    double totalCostBeforeTax; 
    //total cost of purchase before applying the sales tax
    double salesTaxOnPants; 
    //sales tax on pants
    double salesTaxOnShirts; 
    //sales tax on shirts
    double salesTaxOnBelts; 
    //sales tax on belts
    double totalSalesTax; 
    //total sales tax applied to 
    double totalCostOfTrans; 
    //total cost of transaction include tax
    
    
    
    totalCostBeforeTax = ((totalCostOfPants + totalCostOfShirts + totalCostOfBelts) * 100);
    totalCostBeforeTax = (int)totalCostBeforeTax;
    totalCostBeforeTax = totalCostBeforeTax / 100.0; 
    //total cost of all that was purchased before tax
   
    salesTaxOnPants = ((paSalesTax * totalCostOfPants) * 100);
    salesTaxOnPants = (int)salesTaxOnPants;
    salesTaxOnPants = salesTaxOnPants / 100.0; 
    //calculates sales tax on pants by multiplying sales tax by cost of pants
    
    salesTaxOnShirts = ((paSalesTax * totalCostOfShirts) * 100); 
    salesTaxOnShirts = (int)salesTaxOnShirts;
    salesTaxOnShirts = salesTaxOnShirts / 100.0; 
    //calculates sales tax on shirts by multiplying sales tax by cost of shirts
    
    salesTaxOnBelts = ((paSalesTax * totalCostOfBelts) * 100); 
    salesTaxOnBelts = (int)salesTaxOnBelts;
    salesTaxOnBelts = salesTaxOnBelts / 100.0; 
    //calculates sales tax on belts by multiplying sales tax by cost of belts
      
    totalSalesTax = ((salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts) * 100);
    totalSalesTax = (int)totalSalesTax;
    totalSalesTax = totalSalesTax / 100.0;
    //calculates total sales tax by adding all the sales taxes together
    
    totalCostOfTrans = ((totalSalesTax + totalCostBeforeTax) * 100);
    totalCostOfTrans = (int)totalCostOfTrans;
    totalCostOfTrans = totalCostOfTrans / 100.0;
    //calculates the total cost of transaction by adding the total sales tax with the cost before tax is applied
    
    
    System.out.println( "The total cost of pants before tax is $" + totalCostOfPants);
    //prints out the total price of pants before tax
    System.out.println( "The total cost of sweatshirts before tax is $" + totalCostOfShirts);
    //prints out the total price of sweatshirts before tax
    System.out.println( "The total cost of belts before tax $" + totalCostOfBelts);
    //prints out the price of belts before tax 
    System.out.println( "The total cost of the purchase before tax is $" + totalCostBeforeTax);
    //prints out the total cost of the purchase before tax 
    System.out.println( "The sales tax on pants is $" + salesTaxOnPants);
    //prints out the sales tax on pants
    System.out.println( "The sales tax on shirts is $" + salesTaxOnShirts);
    //prints out the sales tax on shirts
    System.out.println( "The sales tax on belts is $" + salesTaxOnBelts);
    //prints out the sales tax on belts 
    System.out.println( "The total sales tax of the purchase is $" + totalSalesTax);
    //prints out the total sales tax of the purchase
    System.out.println( "The total cost of the transaction including sales tax is $" + totalCostOfTrans);
    //prints out the total cost of the transaction including sales tax
    
    
    
    
    
    
    
      
  }
}