//// Michalina Modzelewska
/// CSE002 Hw04
// September 25, 2018

import java.util.Random;
import java.util.Scanner;
public class CrapsSwitch{
  public static void main(String[] args){
     // This program uses nested switch statements to describe the game of Craps and its outcomes.
    
    Random randGen = new Random();
    
    // Integers randomRoll1 and randomRoll2 allow the program to pick a random number from 1 to 6. 
    int randomRoll1 = randGen.nextInt(6) + 1;
    int randomRoll2 = randGen.nextInt(6) + 1;
  
  
    int dice1 = 0; // creates value of the roll of dice 1. 
    int dice2 = 0; // creates value of the roll of dice 2.  
    
    // The following lines of code asks the user for input (using the scanner class) on how they want their dice rolled.
    Scanner myScanner = new Scanner (System.in );
    System.out.println("Do you want to randomly cast the dice (enter 1 if yes, enter 2 if no)?");
      int choice = myScanner.nextInt();
     
      switch(choice){
        case 1:
        dice1 = randomRoll1;
        dice2 = randomRoll2;
      }
             switch(choice){
               case 2: 
                 System.out.println("Do you want to state the dice you want to evaluate (enter 1 if yes, enter 2 if no)");
                 choice = myScanner.nextInt();
       
       switch(choice){
           case 1:
      System.out.println("Enter the first value (between 1 and 6):");
        dice1 = myScanner.nextInt();
           System.out.println("Enter the second value (between 1 and 6):");
        dice2 = myScanner.nextInt();
    }
        switch(choice){
            case 2:
             System.out.println("Please try again and pick an option.");
            break;
          default:
      }
                 break; 
      }
     
 String slangName = " ";
    
    System.out.println("Dice 1 is: " + dice1);
    System.out.println("Dice 2 is: " + dice2);
    
    // The following sets of nested switch statements help determine the values of dice 1 and dice 2 that correspond with a specific slang name. 
   switch(dice1){
   case 1: 
     switch(dice2){
           case 1: 
             slangName = "Snakes Eyes";
             break;
           case 2: 
             slangName = "Ace Deuce";
             break;
           case 3: 
             slangName = "Easy Four ";
             break;
           case 4: 
             slangName = "Fever Five";
             break;
           case 5: 
             slangName = "Easy Six";
             break;
           case 6:
             slangName = "Seven Out";
             break;
         default:
     }
   break;
  case 2:
         switch(dice2){
           case 1:
            slangName = "Ace Deuce";
             break;
           case 2: 
              slangName = "Hard Four";
             break;
           case 3: 
              slangName = "Fever Five";
              break;
           case 4:
               slangName = "Easy Six";
              break; 
           case 5: 
             slangName = "Seven Out";
              break;
           case 6: 
             slangName = "Easy Eight";
              break;
           default: 
         }
 break;
    case 3:
             switch(dice2){
               case 1: 
                 slangName = "Easy Four";
                   break;
               case 2: 
                 slangName = "Fever Five";
                   break;
               case 3: 
                 slangName = "Hard Six";
                 break;
               case 4: 
                 slangName = "Seven Out";
                 break;
               case 5: 
                 slangName = "Easy Eight";
                 break;
               case 6: 
                 slangName = "Nine";
                 break;
               default:
             }
 break;
   case 4: 
                 switch(dice2){
                   case 1: 
                     slangName = "Fever Five";
                       break;
                   case 2:
                     slangName = "Easy Six";
                     break;
                   case 3: 
                     slangName = "Seven Out";
                     break;
                   case 4: 
                     slangName = "Hard Eight";
                     break;
                   case 5: 
                     slangName = "Nine";
                     break;
                   case 6: 
                     slangName = "Easy Ten";
                     break;
                   default:
 }
 break;
    case 5:
                     switch(dice2){
                       case 1:
                         slangName = "Easy Six";
                         break;
                       case 2:
                         slangName = "Seven Out";
                         break;
                       case 3:
                         slangName = "Easy Eight";
                         break;
                       case 4:
                         slangName = "Nine";
                         break;
                       case 5:
                         slangName = "Hard Ten";
                         break;
                       case 6: 
                         slangName = "Yo-leven";
                         break;
                       default:
  }
break;
    case 6:
                         switch(dice2){
                           case 1:
                             slangName = "Seven Out";
                             break;
                           case 2:
                             slangName = "Easy Eight";
                             break;
                           case 3: 
                             slangName = "Nine";
                             break;
                           case 4: 
                             slangName = "Easy Ten";
                             break;
                           case 5: 
                             slangName = "Yo-leven";
                             break;
                           case 6: 
                             slangName = "Boxcars";
                             break; 
                           default:
                             
                      
    }
break;
     default:
   }          
    
     // The statement printed tells the user the result of the roll. 
    System.out.println("You rolled a " + slangName);
      
  }
}