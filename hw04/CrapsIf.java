///// Michalina Modzelewska
//// CSE002 Hw04
/// September 25, 2018
// This program uses nested if statements to describe the game of Craps and its outcomes.

import java.util.Random;
import java.util.Scanner;

public class CrapsIf{
  public static void main(String[] args){
    
 
    Random randGen = new Random();
    
    // Integers randomRoll1 and randomRoll2 allow the program to pick a random number from 1 to 6. 
    int randomRoll1 = randGen.nextInt(6) + 1; 
    int randomRoll2 = randGen.nextInt(6) + 1;
  
  
    int dice1 = 0; // initiates value of the roll of dice 1. 
    int dice2 = 0; // initiates value of the roll of dice 2. 
    
    // The following lines of code asks the user for input (using the scanner class) on how they want their dice rolled.
    Scanner myScanner = new Scanner (System.in );
    System.out.println("Do you want to randomly cast the dice (enter 1 if yes, enter 2 if no)?");
      int choice = myScanner.nextInt();
       if (choice == 1) {
        dice1 = randomRoll1;
        dice2 = randomRoll2;
      }
      else if (choice == 2) {
        System.out.println("Do you want to state the dice you want to evaluate (enter 1 if yes, enter 2 if no)");
       choice = myScanner.nextInt();
        
         if (choice == 1){
      System.out.println("Enter the first value (between 1 and 6):");
        dice1 = myScanner.nextInt();
           System.out.println("Enter the second value (between 1 and 6):");
        dice2 = myScanner.nextInt();
    }
        else if (choice == 2){
          System.out.println("Please try again and pick an option.");
          }
 }
     
 String slangName = " "; // creates string value slang name
    
    System.out.println("Dice 1 is:" + dice1);
    System.out.println("Dice 2 is:" + dice2);
    
    // The following sets of nested if statements help determine the values of dice 1 and dice 2 that correspond with a specific slang name. 
    if (dice1 == 1 && dice2 == 1){
      slangName = "Snake Eyes";
    }
        else if ((dice1 == 1 && dice2 == 2) || (dice1 == 2 && dice2 == 1)){
          slangName = "Ace Deuce";
         }
        else if ((dice1 == 1 && dice2 == 3) || (dice1 == 3 && dice2 == 1)){
              slangName = "Easy Four";
         }
         else if (( dice1 == 1 && dice2 == 4 ) || (dice1 == 4 && dice2 == 1)){
              slangName = "Fever Five";
         }
         else if ((dice1 == 1 && dice2 == 5) || (dice1 == 5 && dice2 == 1)){
              slangName = "Easy Six";
         }
        else if ((dice1 == 1 && dice2 == 6) || (dice1 == 6 && dice2 == 1)){
              slangName = "Seven Out";
        }
     
    
   if (dice1 == 2 && dice2 == 2){
     slangName = "Hard four";
   }
        else if ((dice1 == 2 && dice2 == 3) || (dice1 == 3 && dice2 == 2)){
                slangName = "Fever Five";
        }
        else if ((dice1 == 2 && dice2 == 4) || (dice1 == 4 && dice2 == 2)){
                slangName = "Easy Six";
        }
        else if ((dice1 == 2 && dice2 == 5) || (dice1 == 5 && dice2 == 2)){
                slangName = "Seven Out";
        }
        else if ((dice1 == 2 && dice2 == 6) || (dice1 == 6 && dice2 == 2)){
                slangName = "Easy Eight";
    }
   
    
   if (dice1 == 3 && dice2 == 3){
     slangName = "Hard Six";
   }
        else if ((dice1 == 3 && dice2 == 4) || (dice1 == 4 && dice2 == 3)){
                slangName = "Seven Out";
        }
        else if ((dice1 == 3 && dice2 == 5) || (dice1 == 5 && dice2 == 3)){
                slangName = "Easy Eight";
        }
        else if ((dice1 == 3 && dice2 == 6) || (dice1 == 6 && dice2 == 3)){
                slangName = "Nine";
    }
      
    
   if (dice1 == 4 && dice2 == 4){
     slangName = "Hard Eight";
   }
        else if ((dice1 == 4 && dice2 == 5) || (dice1 == 5 && dice2 == 4)){
                slangName = "Nine";
        }
        else if ((dice1 == 4 && dice2 == 6) || (dice1 == 6 && dice2 == 4 )){
                slangName = "Easy Ten";
        }
    
    
    if (dice1 == 5 && dice2 == 5){
      slangName = "Hard Ten";
    }
        else if ((dice1 == 5 && dice2 == 6) || (dice1 == 6 && dice2 == 5)){
                slangName = "Yo-level";
        } 
    
    
    if (dice1 == 6 && dice2 == 6){
      slangName = "Boxcars";
    }
      
 
  // The statement printed tells the user the result of the roll. 
     System.out.println("You rolled a " + slangName);
  }
  
}