// Michalina Modzelewska
// October 9th, 2018
// CSE002 Hw05
import java.util.Scanner;
// start of the class 
public class TwistGenerator{
  // start of the main method
  public static void main (String [] args) {
    
    Scanner myScanner = new Scanner(System.in);

    
    int length; // initiliazing the length of the twist
 
    String junk; // initializing the string junk which is used to re-enter values if they were first entered incorrectly by the user 
    
    // Asking for user input, and if the user enters a variable that isn't an integer it will yield an error
    System.out.println("Please provide the length:");
    while (!myScanner.hasNextInt()){
      System.out.println("Error, please enter a positive integer.");
        junk = myScanner.nextLine();
    }
    
    length = myScanner.nextInt();
    int i; // initializing i lets the program 
    
   // Twists are 3 lines long. This is why length is being divided by 3 in the following for loops 
   // Each for loop prints the needed pattern
   for (i = length/3; i > 0; i--) {
      System.out.print(" \\ /");
     }
   // If the division by 3 yields a remainder, a different pattern needs to be printed to accommodate for the length of a twist
   if (length % 3 > 0) { 
     System.out.print(" \\ ");
    }
   
   System.out.println();
   
   for (i = length/3; i > 0; i--) {
     System.out.print("  X ");
    }
   
   System.out.println();
    
    for (i = length/3; i > 0; i--) {
      System.out.print(" / \\");
    }
    if (length % 3 > 0) {
     System.out.print(" / ");
    }
    System.out.println();
    
  }
}