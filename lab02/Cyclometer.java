//////////// Michalina Modzelewska
/////// September 5th, 2018
///// CSE2 Lab02 "Arithmetic Calculations" with a cyclometer 
/// The program will measure the speed and distance of a bicycle by recording the time elapsed (in seconds) and the number of rotations of the front wheel during the time elapsed

  public class Cyclometer{
  
    public static void main(String args[]){
     
      int secsTrip1 = 480; // Duration of the first trip in seconds
      int secsTrip2 = 3220; // Duration of the second trip in seconds
      int countsTrip1 = 1561; // Number of rotations per total time of first trip in "counts"
      int countsTrip2 = 9037; // Number of rotations per total time of second trip in "counts"
      
      double wheelDiameter = 27.0, // Bicycle wheel diameter
      PI = 3.14159, // Value of pi
      feetPerMile = 5280, // How many feet equal one mile
      inchesPerFoot = 12, // How many inches equal one foot
      secondsPerMinute = 60; // How many seconds equal one minute
      double distanceTrip1, distanceTrip2, totalDistance; // Distances of trip1 and trip2 and the total distance travelled between both trips
      
      System.out.println("Trip 1 took "+ 
                        (secsTrip1 / secondsPerMinute) +" minutes and had "+
                        countsTrip1+" counts.");
      System.out.println("Trip 2 took "+
                        (secsTrip2 / secondsPerMinute) +" minutes and had "+  
                        countsTrip2+" counts.");
      //////// Below calculations are being run to calculate the distances of each trip by multiplying 
      ///// the rotations of the wheel per trip by the diameter of the wheel and then by the value of pi. 
      /// The total distance is also being calculated by adding together the distances 
      // from trip 1 and trip 2.
          
      distanceTrip1 = countsTrip1 * wheelDiameter * PI;
      // Above gives distance in inches 
      //(for each count, a rotation of the wheel travels
      //the diameter in inches times PI)
      distanceTrip1 = inchesPerFoot * feetPerMile; // Gives distance in miles
      distanceTrip2 = countsTrip2 * wheelDiameter * PI; //inchesPerFoot/feetperMile;
        totalDistance = distanceTrip1 + distanceTrip2;
        
        //Print out the output data
        System.out.println("Trip 1 was "+distanceTrip1+" miles");
      System.out.println("Trip 2 was "+distanceTrip2+" miles");
        System.out.println("The total distance was "+totalDistance+" miles");
   
          
        
       
    } //end of main method
  } //end of class