//// Michalina Modzelewska
/// CSE02 Lab04
// September 19th, 2018

// This program picks a random number card from a deck of 52 cards. There are 4 suits in a deck of cards. 
// It uses a random number generator to select a number from 1 to 52 (inclusive).  
// Each number represents one card, and the suits are grouped:  
// Cards 1-13 represent the diamonds, 14-26 represent the clubs, then hearts, then spades.  



import java.util.Random;

public class CardGenerator{
  public static void main(String[] args){
   
   // generates a random card number 
    Random randGen = new Random();
    int randomCard = randGen.nextInt(52) + 1;
    
    String suit = " "; 
    String identity = " ";
   
   // each number represents one card, and the suits are groupes so that cards 1-13 represent the diamonds, 14-26 represent the clubs, then hearts, then spades.  
    if ( randomCard >= 1 && randomCard <= 13) {
       randomCard = 52 - randomCard;
        suit = "diamonds";
    }
     else if (randomCard >= 14 && randomCard <= 26) {
         randomCard = 52 - randomCard;
        suit = "clubs";
    }
     else if (randomCard >= 27 && randomCard <= 39) {
         randomCard = 52 - randomCard;
        suit = "hearts";
    }
     else if (randomCard >= 40 && randomCard <= 52) {
        randomCard = 52 - randomCard;
        suit = "spades";
    }
    
    // here switch statements and the modulo are used to find the identity of each card.
    // For example, if card number 30 is picked, 30 % 13 would yield case 4. 
        switch (randomCard % 13) {
     
          case 0: identity = "Ace";
            break;
          case 1: identity = "2";
            break;
          case 2: identity = "3";
            break;
          case 3: identity = "4";
            break;
          case 4: identity = "5";
            break;
          case 5: identity = "6";
            break;
          case 6: identity = "7";
            break;
          case 7: identity = "8";
            break;
          case 8: identity = "9";
            break;
          case 9: identity = "10";
            break;
          case 10: identity = "Jack";
            break;
          case 11: identity = "Queen";
            break;
          case 12: identity = "King";
            break;
          default:
            break;
            
        }
            // here the identity and suit of the card picked is printed out.
            // looking back at the previous example with card 30 (which yields an identity of 2), the 30 signifies a suit of hearts. 
            System.out.println("You picked the " + identity + " of " + suit);
       
    }
   
  }