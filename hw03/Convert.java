/////// Michalina Modzelewska 
///// CSE002 Hw03 
/// September 18th, 2018
import java.util.Scanner;

public class Convert{
  
  public static void main(String args[]){
    
    // This program uses the scanner class to figure out the quantity of rainfall from a hurricane over a specified area in cubic miles.
    Scanner myScanner = new Scanner (System.in );
    
    System.out.print("Enter the affected area in acres");
    
    // This represents the acres of land affected by the hurricane. 
    double acresLand = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in affected area");
    
    // This represents the amount of rainfall in the affected area, given in inches by the user.
    double avgRain = myScanner.nextInt();
    
    // We want to convert the average rainfall from inches to cubic miles. The
    avgRain = avgRain * acresLand;
    
    // Note that 1 acre inch = 27154.2857 gallons.
    avgRain = avgRain * 27154.2857;
    
    // Note that 1 gallon = 9.08169e-13 cubic miles.
    avgRain = avgRain * (9.08169 * Math.pow(10, -13));
    
    System.out.print( avgRain + " cubic miles ");
  }
}