//// Michalina Modzelewska
// CSE002 hw03
// September 18th, 2018
import java.util.Scanner;

public class Pyramid{
  
  public static void main(String[] args){
    
    // This program will calculate the volume inside of a period when the user inputs the length of a square side and the height of the pyramid.
    Scanner myScanner = new Scanner (System.in );
    
    System.out.print("The square side of the pyramid is (input length): ");
   
    // This represents the square side of the pyramid.
    double squareSide = myScanner.nextDouble();
      
    System.out.print("The height of a pyramid is (input height): ");
    
    // This represents the height of the pyramid. 
    double height = myScanner.nextDouble();
    
    // This represents the volume inside of the pyramid. 
    double volume; 
    
    // Note that the formula for the volume of a pyramid (excluding width) is ((base*height)/3). 
    volume = Math.pow( squareSide, 2); 
    volume = volume * height; 
    volume = volume / 3;
      
    System.out.print("The volume inside the pyramid is: " + volume);
  }
    
}