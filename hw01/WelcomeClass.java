//////////// 
////// CSE02 Welcome Class
///

public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints "Welcome" and the desired Lehigh user ID to the terminal window with specific characters surrounding the words
    System.out.println(" ----------- ");
    System.out.println(" | WELCOME |" );
    System.out.println(" ----------- ");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--I--M--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");                  
    System.out.println("  v  v  v  v  v  v");
                                        
  }
}

//////// I'm Michalina Modzelewska and my major is CSB. I'm from South Amboy, New Jersey and I love to play tennis. 
/// 